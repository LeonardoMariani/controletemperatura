create database ControleTemp;
use ControleTemp;

create table Perfil(
	codPerfil int primary key auto_increment,
    nome varchar(30) unique not null
);
create table PontosOperacao(
	codigo int primary key auto_increment,
    codPerfil int not null,
    ldrMin float not null,
    ldrMax float not null,
    PWM float not null,
    foreign key(codPerfil) references Perfil(codPerfil)
);
create table usoPerfil(
	codUsoPerfil int auto_increment primary key,
	codPerfil int,
    tempoInicio datetime not null,
    tempoFim datetime,
    foreign key (codPerfil) references Perfil(codPerfil)
);
create table Dados(
	codigo int primary key auto_increment,
    codPerfil int not null,
    valorLDR float not null,
    valorPWM float not null,
    potenciaMedia float not null,
    codUsoPerfil int not null,
    foreign key (codUsoPerfil) references usoPerfil(codUsoPerfil),
    foreign key(codPerfil) references Perfil(codPerfil)
);

insert into Dados values("2","1","10","50","90","1");
insert into Perfil values("1","P1");
insert into Perfil values("2","P2");
insert into Perfil values("3","P3");
insert into usoPerfil values("1","2017-09-26 11:00:00","2017-09-26 11:25:00");
insert into usoPerfil values("3","2017-09-24 05:25:00.000000","2017-09-24 05:30:00.000000");
-- perfil ( nomePerfil -- tempoOp)
 select distinct Perfil.nome as `Nome Perfil`,(select  sec_to_time((SUM(time_to_sec(usoPerfil.tempoFim) - time_to_sec(usoPerfil.tempoInicio))))
                           from usoPerfil
                           where Perfil.codPerfil = usoPerfil.codPerfil
                           ) as `Tempo ativo`
                           from usoPerfil
                           inner join Perfil
                           on usoPerfil.codPerfil = Perfil.codPerfil;
                           
-- consumo ( nomePerfil -- potenciaMedia -- tempoOp)
 select distinct Dados.potenciaMedia as `Potência média`, Perfil.nome as `Nome Perfil`,(select  sec_to_time((SUM(time_to_sec(usoPerfil.tempoFim) - time_to_sec(usoPerfil.tempoInicio))))
from usoPerfil
where Perfil.codPerfil = usoPerfil.codPerfil  or Dados.codUsoPerfil = usoperfil.codUsoPerfil
) as `Tempo total do perfil`
from usoPerfil
inner join Perfil
on usoPerfil.codPerfil = Perfil.codPerfil
inner join Dados
on Dados.codPerfil = Perfil.codPerfil;

-- periodo (nomePerfil)
select distinct Perfil.nome from Perfil
inner join usoPerfil
on Perfil.codPerfil = usoPerfil.codPerfil
where usoperfil.tempoInicio >= "2017-10-02 00:00:00" and usoperfil.tempoFim <= "2017-10-04 00:00:00";

-- Relatorio pika
select * from usoperfil;
select * from dados limit 0, 10000;
select * from perfil;
update usoPerfil set tempoFim = "2017-10-05 16:45:00" where codUsoPerfil = 7;
select codUsoPerfil from usoPerfil order by codUsoPerfil desc limit 1;
select codPerfil from perfil where nome = "abcdefghijkkas";

select dados.potenciaMedia,perfil.nome, usoperfil.tempoInicio, usoperfil.tempofim
from usoperfil
inner join dados
on usoperfil.codUsoPerfil = dados.codUsoPerfil and usoperfil.tempoInicio > "2017-10-02 00:00:00" and usoperfil.tempoFim < "2017-10-08 00:00:00"
inner join perfil
on perfil.codPerfil = usoperfil.codPerfil;


insert into dados(codPerfil, valorLDR, valorPWM, potenciaMedia, codUsoPerfil) values("4", "50","90","100","5");
insert into usoPerfil(codPerfil,tempoInicio, tempoFim) values("1","2017-10-03 05:25:00.000000","2017-10-03 05:30:00.000000");
insert into usoPerfil(codPerfil,tempoInicio, tempoFim) values("2","2017-10-03 05:31:00.000000","2017-10-03 05:40:00.000000");
insert into usoPerfil(codPerfil,tempoInicio, tempoFim) values("1","2017-10-03 05:45:00.000000","2017-10-03 06:00:00.000000");
insert into usoPerfil(codPerfil,tempoInicio, tempoFim) values("2","2017-10-03 06:01:00.000000","2017-10-03 06:41:00.000000");
insert into usoPerfil(codPerfil,tempoInicio, tempoFim) values("4","2017-10-03 07:00:00.000000","2017-10-03 07:30:00.000000");

select dados.potenciaMedia,perfil.nome
from usoperfil 
inner join dados
on usoperfil.codUsoPerfil = dados.codUsoPerfil and usoperfil.tempoInicio >= "2017-10-02 00:00:00" and usoperfil.tempoFim <= "2017-10-04 00:00:00"
inner join perfil
on perfil.codPerfil = usoperfil.codPerfil;



