﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_temperatura.MODEL
{
    class PontosOpModel
    {
        private int codigo;
        private int codigoPerfil;
        private int ldrMin;
        private int ldrMax;
        private int PWM;

        public int GetCodigo()
        {
            return codigo;
        }
        public void SetCodigo(int value)
        {
            codigo = value;
        }

        public int GetCodigoPerfil()
        {
            return codigoPerfil;
        }
        public void SetCodigoPerfil(int value)
        {
            codigoPerfil = value;
        }

        public int GetLdrMin()
        {
            return ldrMin;
        }
        public void SetLdrMin(int value)
        {
            ldrMin = value;
        }

        public int GetLdrMax()
        {
            return ldrMax;
        }
        public void SetLdrMax(int value)
        {
            ldrMax = value;
        }
      
        public int GetPWM()
        {
            return PWM;
        }
        public void SetPWM(int value)
        {
            PWM = value;
        }
    }
}
