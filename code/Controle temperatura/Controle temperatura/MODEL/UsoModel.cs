﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_temperatura.MODEL
{
    class UsoModel
    {
        private int codigo;
        private DateTime tempoInicio;
        private DateTime tempoFim;

        public int GetCodigo()
        {
            return codigo;
        }
        public void SetCodigo(int value)
        {
            codigo = value;
        }

        public DateTime GetTempoInicio()
        {
            return tempoInicio;
        }
        public void SetTempoInicio(DateTime value)
        {
            tempoInicio = value;
        }

        public DateTime GetTempoFim()
        {
            return tempoFim;
        }
        public void SetTempoFim(DateTime value)
        {
            tempoFim = value;
        }
    }
}
