﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_temperatura.MODEL
{
    class DadosModel
    {
        private int codigo;
        private int codPerfil;
        private float leituraLDR;
        private float leituraPWM;
        private float potenciaMedia;
        private int codUsoPerfil;

        public int GetCodigo()
        {
            return codigo;
        }
        public void SetCodigo(int value)
        {
            codigo = value;
        }

        public int GetCodPerfil()
        {
            return codPerfil;
        }
        public void SetCodPerfil(int value)
        {
            codPerfil = value;
        }

        public float GetLeituraLDR()
        {
            return leituraLDR;
        }
        public void SetLeituraLDR(float value)
        {
            leituraLDR = value;
        }

        public float GetLeituraPWM()
        {
            return leituraPWM;
        }
        public void SetLeituraPWM(float value)
        {
            leituraPWM = value;
        }

        public float GetPotenciaMedia()
        {
            return potenciaMedia;
        }
        public void SetPotenciaMedia(float value)
        {
            potenciaMedia = value;
        }

        public int GetCodUsoPerfil()
        {
            return codUsoPerfil;
        }
        public void SetCodUsoPerfil(int value)
        {
            codUsoPerfil = value;
        }
    }
}
