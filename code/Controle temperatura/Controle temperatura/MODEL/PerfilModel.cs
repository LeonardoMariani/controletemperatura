﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_temperatura.MODEL
{
    class PerfilModel
    {
        private int Codigo;
        private string Nome;

        public int GetCodigo()
        {
            return Codigo;
        }
        public void SetCodigo(int Codigo)
        {
            this.Codigo = Codigo;
        }

        public string GetNome()
        {
            return Nome;
        }
        public void SetNome(string Nome)
        {
            this.Nome = Nome;
        }
    }
}
