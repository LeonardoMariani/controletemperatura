﻿namespace Controle_temperatura.VIEW
{
    partial class viewCadastro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonPontos = new System.Windows.Forms.Button();
            this.buttonCancelar = new System.Windows.Forms.Button();
            this.buttonSalvar = new System.Windows.Forms.Button();
            this.maskedTextBoxLDRMin = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxLDRMax = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxPWM = new System.Windows.Forms.MaskedTextBox();
            this.labelAviso1 = new System.Windows.Forms.Label();
            this.labelAviso2 = new System.Windows.Forms.Label();
            this.labelLDRMin = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            // 
            // textBoxNome
            // 
            this.textBoxNome.Location = new System.Drawing.Point(68, 18);
            this.textBoxNome.Name = "textBoxNome";
            this.textBoxNome.Size = new System.Drawing.Size(168, 20);
            this.textBoxNome.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "LDR min (%):";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "LDR max (%):";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Potência motor (%):";
            // 
            // buttonPontos
            // 
            this.buttonPontos.Location = new System.Drawing.Point(180, 70);
            this.buttonPontos.Name = "buttonPontos";
            this.buttonPontos.Size = new System.Drawing.Size(92, 95);
            this.buttonPontos.TabIndex = 5;
            this.buttonPontos.Text = "Adicionar ponto de operação";
            this.buttonPontos.UseVisualStyleBackColor = true;
            this.buttonPontos.Click += new System.EventHandler(this.buttonPontos_Click);
            // 
            // buttonCancelar
            // 
            this.buttonCancelar.Location = new System.Drawing.Point(27, 206);
            this.buttonCancelar.Name = "buttonCancelar";
            this.buttonCancelar.Size = new System.Drawing.Size(95, 36);
            this.buttonCancelar.TabIndex = 7;
            this.buttonCancelar.Text = "Cancelar";
            this.buttonCancelar.UseVisualStyleBackColor = true;
            this.buttonCancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // buttonSalvar
            // 
            this.buttonSalvar.Location = new System.Drawing.Point(177, 206);
            this.buttonSalvar.Name = "buttonSalvar";
            this.buttonSalvar.Size = new System.Drawing.Size(95, 36);
            this.buttonSalvar.TabIndex = 6;
            this.buttonSalvar.Text = "Salvar";
            this.buttonSalvar.UseVisualStyleBackColor = true;
            this.buttonSalvar.Click += new System.EventHandler(this.buttonSalvar_Click);
            // 
            // maskedTextBoxLDRMin
            // 
            this.maskedTextBoxLDRMin.Enabled = false;
            this.maskedTextBoxLDRMin.Location = new System.Drawing.Point(128, 74);
            this.maskedTextBoxLDRMin.Mask = "000%";
            this.maskedTextBoxLDRMin.Name = "maskedTextBoxLDRMin";
            this.maskedTextBoxLDRMin.Size = new System.Drawing.Size(33, 20);
            this.maskedTextBoxLDRMin.TabIndex = 2;
            this.maskedTextBoxLDRMin.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            this.maskedTextBoxLDRMin.Leave += new System.EventHandler(this.maskedTextBoxLDRMin_Leave);
            // 
            // maskedTextBoxLDRMax
            // 
            this.maskedTextBoxLDRMax.Location = new System.Drawing.Point(127, 108);
            this.maskedTextBoxLDRMax.Mask = "000%";
            this.maskedTextBoxLDRMax.Name = "maskedTextBoxLDRMax";
            this.maskedTextBoxLDRMax.Size = new System.Drawing.Size(33, 20);
            this.maskedTextBoxLDRMax.TabIndex = 3;
            this.maskedTextBoxLDRMax.Leave += new System.EventHandler(this.maskedTextBoxLDRMax_Leave);
            // 
            // maskedTextBoxPWM
            // 
            this.maskedTextBoxPWM.Location = new System.Drawing.Point(127, 145);
            this.maskedTextBoxPWM.Mask = "000%";
            this.maskedTextBoxPWM.Name = "maskedTextBoxPWM";
            this.maskedTextBoxPWM.Size = new System.Drawing.Size(33, 20);
            this.maskedTextBoxPWM.TabIndex = 4;
            this.maskedTextBoxPWM.Leave += new System.EventHandler(this.maskedTextBoxPWM_Leave);
            // 
            // labelAviso1
            // 
            this.labelAviso1.AutoSize = true;
            this.labelAviso1.ForeColor = System.Drawing.Color.Red;
            this.labelAviso1.Location = new System.Drawing.Point(12, 254);
            this.labelAviso1.Name = "labelAviso1";
            this.labelAviso1.Size = new System.Drawing.Size(264, 13);
            this.labelAviso1.TabIndex = 14;
            this.labelAviso1.Text = "O valor do LDRMin deve ser menor do que o LDRMax";
            this.labelAviso1.Visible = false;
            // 
            // labelAviso2
            // 
            this.labelAviso2.AutoSize = true;
            this.labelAviso2.ForeColor = System.Drawing.Color.Red;
            this.labelAviso2.Location = new System.Drawing.Point(12, 281);
            this.labelAviso2.Name = "labelAviso2";
            this.labelAviso2.Size = new System.Drawing.Size(201, 13);
            this.labelAviso2.TabIndex = 15;
            this.labelAviso2.Text = "O valor maximo dos LDR e PWM é 100%";
            this.labelAviso2.Visible = false;
            // 
            // labelLDRMin
            // 
            this.labelLDRMin.AutoSize = true;
            this.labelLDRMin.Location = new System.Drawing.Point(32, 54);
            this.labelLDRMin.Name = "labelLDRMin";
            this.labelLDRMin.Size = new System.Drawing.Size(35, 13);
            this.labelLDRMin.TabIndex = 16;
            this.labelLDRMin.Text = "label5";
            // 
            // viewCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 325);
            this.Controls.Add(this.labelLDRMin);
            this.Controls.Add(this.labelAviso2);
            this.Controls.Add(this.labelAviso1);
            this.Controls.Add(this.maskedTextBoxPWM);
            this.Controls.Add(this.maskedTextBoxLDRMax);
            this.Controls.Add(this.maskedTextBoxLDRMin);
            this.Controls.Add(this.buttonSalvar);
            this.Controls.Add(this.buttonCancelar);
            this.Controls.Add(this.buttonPontos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxNome);
            this.Controls.Add(this.label1);
            this.Name = "viewCadastro";
            this.Text = "Cadastro";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.viewCadastro_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.viewCadastro_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonPontos;
        private System.Windows.Forms.Button buttonCancelar;
        private System.Windows.Forms.Button buttonSalvar;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxLDRMin;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxLDRMax;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxPWM;
        private System.Windows.Forms.Label labelAviso1;
        private System.Windows.Forms.Label labelAviso2;
        private System.Windows.Forms.Label labelLDRMin;
    }
}