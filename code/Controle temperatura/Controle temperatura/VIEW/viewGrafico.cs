﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controle_temperatura.CONTROLLER;
using MySql.Data.MySqlClient;
using System.Windows.Forms.DataVisualization.Charting;

namespace Controle_temperatura.VIEW {
    public partial class viewGrafico : Form {

        RelatorioController controller;
        DataTable reader;
        List<String> listaNomes;
        List<String> listaPotencias;
        Series linhasGrafico = null;

        public viewGrafico() {
            InitializeComponent();
            controller = new RelatorioController();
            reader = new DataTable();
            listaNomes = new List<String>();
            listaPotencias = new List<String>();
        }

        private void button1_Click(object sender, EventArgs e) {

            if(dateTimePickerInicio.Value <= dateTimePickerFim.Value) {

                reader = controller.getGrafico(dateTimePickerInicio.Value.AddDays(-1), dateTimePickerFim.Value.AddDays(1));
                
                if(reader == null) {
                    MessageBox.Show("Erro");
                } else {
                    setListas();
                    chart1.DataSource = reader;
                    if(linhasGrafico == null) {
                        linhasGrafico = chart1.Series.Add("Perfil ativo");
                        chart1.ChartAreas[0].AxisY.Maximum = 100;
                        linhasGrafico.Points.DataBindXY(listaNomes, listaPotencias);
                        linhasGrafico.ChartType = SeriesChartType.StepLine;
                        linhasGrafico.Color = Color.Red;
                        linhasGrafico.BorderWidth = 3;
                    }
                }
                

            } else {
                MessageBox.Show("A data de inicio deve ser menor do que a data final");
            }
        }

        private void setListas() {
            //add nomes
            for(int i = 0; i < reader.Rows.Count; i++) {
                listaNomes.Add(reader.Rows[i].ItemArray[1].ToString());
            }
            //add potencia media
            for(int i = 0; i < reader.Rows.Count; i++) {
                listaPotencias.Add(reader.Rows[i].ItemArray[0].ToString());
            }
        }

        private void buttonVoltar_Click(object sender, EventArgs e) {
            foreach (var series in chart1.Series) {
                //series.Points.Clear();
            }
            while (chart1.Series.Count > 0) { chart1.Series.RemoveAt(0); }
            listaNomes.Clear();
            listaPotencias.Clear();
            this.Hide();
        }
    }
}
