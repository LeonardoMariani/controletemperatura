﻿using Controle_temperatura.CONTROLLER;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controle_temperatura.VIEW {
    public partial class viewRelatorio : Form {

        DataTable relatorio;
        RelatorioController relatorioController;

        public viewRelatorio() {
            InitializeComponent();
            relatorio  = new DataTable();
            relatorioController = new RelatorioController();

            dateTimePickerInicio.Format = DateTimePickerFormat.Custom;
            dateTimePickerInicio.CustomFormat = "dd/MM/yyyy";

            dateTimePickerFim.Format = DateTimePickerFormat.Custom;
            dateTimePickerFim.CustomFormat = "dd/MM/yyyy";
        }

        private void comboBoxFiltro_SelectedIndexChanged(object sender, EventArgs e) {

            dataGridView1.DataSource = null;
            if (comboBoxFiltro.Text == "Período") {
                dateTimePickerInicio.Enabled = true;
                dateTimePickerFim.Enabled = true;
            } else {
                dateTimePickerInicio.Enabled = false;
                dateTimePickerFim.Enabled = false;
            }
        }

        private void buttonFiltrar_Click(object sender, EventArgs e) {

            relatorio.Columns.Clear();
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Refresh();
            if (comboBoxFiltro.SelectedIndex == 0) {//Consumo
                relatorioConsumo();
            } else if (comboBoxFiltro.SelectedIndex == 1) {//Perfil
                relatorioPerfil();
            } else if (comboBoxFiltro.SelectedIndex == 2) {//Periodo
                relatorioPeriodo();
            }            
        }

        private void relatorioPeriodo() {

            relatorio = relatorioController.getPeriodo(dateTimePickerInicio.Value, dateTimePickerFim.Value);

            if(relatorio.Rows.Count > 0) {
                dataGridView1.DataSource = relatorio;
            } else{
                MessageBox.Show("Não ha dados.");
            }
        }

        private void relatorioConsumo() {

            relatorio = relatorioController.getConsumo();

            if(relatorio.Rows.Count > 0) {
                dataGridView1.DataSource = relatorio;
            } else {
                MessageBox.Show("Não ha dados.");
            }
        }

        private void relatorioPerfil() {

            relatorio = relatorioController.getPerfil();

            if(relatorio.Rows.Count > 0) {
                dataGridView1.DataSource = relatorio;
            } else {
                MessageBox.Show("Não ha dados.");
            }
        }

        private void viewRelatorio_FormClosing(object sender, FormClosingEventArgs e) {
            relatorio.Clear();
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
        }
    }
}
