﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controle_temperatura.VIEW;
using Controle_temperatura.MODEL;
using Controle_temperatura.CONTROLLER;
using System.IO.Ports;
using System.Threading;

namespace Controle_temperatura
{
    public partial class Form1 : Form
    {
        viewCadastro telaCadastro;
        viewRelatorio telaRelatorio;
        viewGrafico graficoView;
        List<String> listaPerfil;
        PerfilModel perfilModel;
        PerfilController perfilController;
        UsoPerfilController usoPerfilController;
        UsoModel usoPerfilModel;
        SerialPort serialPort;
        List<String> listaPortas;
        SerialCommController serialController;
        bool serialOk;
        int codUsoPerfilAnterior = 0;
        int codUsoPerfilAtual = 0;
        int codPerfil;

        public Form1()
        {
            InitializeComponent();
            telaCadastro = new viewCadastro();
            telaRelatorio = new viewRelatorio();
            graficoView = new viewGrafico();
            listaPerfil = new List<String>();
            perfilModel = new PerfilModel();
            perfilController = new PerfilController();
            usoPerfilController = new UsoPerfilController();
            serialPort = new SerialPort();
            listaPortas = new List<string>();
            serialController = new SerialCommController();
            usoPerfilModel = new UsoModel();
            labelStatus.Text = "Desconectado";
            labelStatus.ForeColor = Color.Red;
            carregarPortas();
            carregarCombobox();
        }

        private void carregarPortas() {
            string[] lista = null;

            lista = SerialPort.GetPortNames();

            for(int i = 0; i < lista.Count(); i++) {
                //listaPortas.Add(lista.ToString());
                comboBoxPortas.Items.Add(lista[i].ToString());
            }
            comboBoxPortas.SelectedIndex = 0;
        }

        private void buttonNovoPerfil_Click(object sender, EventArgs e)
        {
            telaCadastro.ShowDialog();
            carregarCombobox();
        }

        private void carregarCombobox(){

            comboBoxPerfil.Items.Clear();
            listaPerfil = perfilController.getNomePerfil();

            bool flag = true;
            if(listaPerfil.Count > 0) {
                for(int i =0; i < listaPerfil.Count; i++) {

                    comboBoxPerfil.Items.Add(listaPerfil[i]);                
                }
            }
            comboBoxPerfil.Items.Add("Genérico");
            comboBoxPerfil.SelectedIndex = 0;
        }

        private void buttonRelatorio_Click(object sender, EventArgs e)
        {
            telaRelatorio.ShowDialog();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {

        }

        private void Form1_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Form1_Shown(object sender, EventArgs e)
        {

        }

        private void buttonConectar_Click(object sender, EventArgs e) {

            if(comboBoxPortas.SelectedIndex != -1) {
                serialPort.Close();
                serialController.stop();
                serialPort.PortName = comboBoxPortas.Text;
                serialPort.BaudRate = 9600;
                serialPort.DataBits = 8;
                try {
                    if(!serialPort.IsOpen) serialPort.Open();
                    serialOk = true;
                    //serialController.startThread(comboBoxPortas.Text,9600,8);
                    serialController.start();
                    serialController.startThread(serialPort);

                } catch(Exception a) {
                    serialOk = false;
                }

                serialPort.Write("!");//iniciar conexao
                Thread.Sleep(150);
                //serialPort.ReadChar();
                String serialInput = serialPort.ReadExisting();

                if (serialInput == String.Empty) {
                    for(int i = 0; i < 10; i++) {
                        serialPort.Write("!");//iniciar conexao                        
                    }
                }
                if (serialInput == "!") {
                    MessageBox.Show("Hey");
                }
            }

            bool conectado = serialController.getConectado();

            if (conectado) {
                labelStatus.Text = "Conectado";
                labelStatus.ForeColor = Color.Green;
            } else {
                labelStatus.Text = "Desconectado";
                labelStatus.ForeColor = Color.Red;
            }
        }

        private void buttonCarregar_Click(object sender, EventArgs e) {
            if (serialOk) {
                novoUsoPerfil();
                codUsoPerfilAtual = codUsoPerfilAnterior;
                string nomePerfil = comboBoxPerfil.Text;
                serialController.setPerfil(nomePerfil);
                serialController.setCodPerfil(codPerfil);
                serialController.setUsoPerfil(codUsoPerfilAtual);
                serialPort.Write(";");//enviar perfil
                Thread.Sleep(500);
                for(int i = 0; i < nomePerfil.Length; i++) {
                    serialPort.Write(Convert.ToString(nomePerfil.ElementAt(i)));
                    Thread.Sleep(200);
                }
                serialController.setPerfil(nomePerfil);
                for(int i = 0; i < 5; i++) {
                    serialPort.Write("*");//fim de palavra
                    Thread.Sleep(200);
                }
            } else {
                MessageBox.Show("Erro ao conectar com a porta serial");
            }
        }

        private void novoUsoPerfil() {
            //criar uso perfil
            if (codUsoPerfilAnterior != 0) {
                DateTime tempoFim = DateTime.Now;
                usoPerfilController.addTempoFim(codUsoPerfilAnterior, tempoFim);
            }
            codPerfil = perfilController.getCodPerfil(comboBoxPerfil.Text);
            DateTime tempoInicio = DateTime.Now;

            usoPerfilModel.SetCodigo(codPerfil);
            usoPerfilModel.SetTempoInicio(tempoInicio);
            codUsoPerfilAnterior = usoPerfilController.novoUso(usoPerfilModel);
        }

        private void button1_Click(object sender, EventArgs e) {
            graficoView.ShowDialog();
        }

        private void comboBoxPortas_SelectedIndexChanged(object sender, EventArgs e) {
        }

        private void comboBoxPerfil_SelectedIndexChanged(object sender, EventArgs e) {


        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e) {

            DateTime tempoFim = DateTime.Now;
            usoPerfilController.addTempoFim(codUsoPerfilAnterior, tempoFim);
        }
    }
}
