﻿namespace Controle_temperatura
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxPerfil = new System.Windows.Forms.ComboBox();
            this.buttonNovoPerfil = new System.Windows.Forms.Button();
            this.buttonRelatorio = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.buttonCarregar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxPortas = new System.Windows.Forms.ComboBox();
            this.buttonConectar = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Selecione um perfil";
            // 
            // comboBoxPerfil
            // 
            this.comboBoxPerfil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPerfil.FormattingEnabled = true;
            this.comboBoxPerfil.Location = new System.Drawing.Point(29, 78);
            this.comboBoxPerfil.Name = "comboBoxPerfil";
            this.comboBoxPerfil.Size = new System.Drawing.Size(235, 21);
            this.comboBoxPerfil.TabIndex = 1;
            this.comboBoxPerfil.SelectedIndexChanged += new System.EventHandler(this.comboBoxPerfil_SelectedIndexChanged);
            // 
            // buttonNovoPerfil
            // 
            this.buttonNovoPerfil.Location = new System.Drawing.Point(171, 189);
            this.buttonNovoPerfil.Name = "buttonNovoPerfil";
            this.buttonNovoPerfil.Size = new System.Drawing.Size(93, 46);
            this.buttonNovoPerfil.TabIndex = 2;
            this.buttonNovoPerfil.Text = "Criar novo perfil";
            this.buttonNovoPerfil.UseVisualStyleBackColor = true;
            this.buttonNovoPerfil.Click += new System.EventHandler(this.buttonNovoPerfil_Click);
            // 
            // buttonRelatorio
            // 
            this.buttonRelatorio.Location = new System.Drawing.Point(29, 189);
            this.buttonRelatorio.Name = "buttonRelatorio";
            this.buttonRelatorio.Size = new System.Drawing.Size(93, 46);
            this.buttonRelatorio.TabIndex = 3;
            this.buttonRelatorio.Text = "Exibir relatorios";
            this.buttonRelatorio.UseVisualStyleBackColor = true;
            this.buttonRelatorio.Click += new System.EventHandler(this.buttonRelatorio_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Status do controlador:";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(144, 9);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(85, 13);
            this.labelStatus.TabIndex = 5;
            this.labelStatus.Text = "ligado/desligado";
            // 
            // buttonCarregar
            // 
            this.buttonCarregar.Location = new System.Drawing.Point(29, 115);
            this.buttonCarregar.Name = "buttonCarregar";
            this.buttonCarregar.Size = new System.Drawing.Size(233, 43);
            this.buttonCarregar.TabIndex = 6;
            this.buttonCarregar.Text = "Carregar perfil";
            this.buttonCarregar.UseVisualStyleBackColor = true;
            this.buttonCarregar.Click += new System.EventHandler(this.buttonCarregar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 268);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Selecione uma porta";
            // 
            // comboBoxPortas
            // 
            this.comboBoxPortas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPortas.FormattingEnabled = true;
            this.comboBoxPortas.Location = new System.Drawing.Point(33, 302);
            this.comboBoxPortas.Name = "comboBoxPortas";
            this.comboBoxPortas.Size = new System.Drawing.Size(228, 21);
            this.comboBoxPortas.TabIndex = 8;
            this.comboBoxPortas.SelectedIndexChanged += new System.EventHandler(this.comboBoxPortas_SelectedIndexChanged);
            // 
            // buttonConectar
            // 
            this.buttonConectar.Location = new System.Drawing.Point(37, 342);
            this.buttonConectar.Name = "buttonConectar";
            this.buttonConectar.Size = new System.Drawing.Size(223, 40);
            this.buttonConectar.TabIndex = 9;
            this.buttonConectar.Text = "Conectar";
            this.buttonConectar.UseVisualStyleBackColor = true;
            this.buttonConectar.Click += new System.EventHandler(this.buttonConectar_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(37, 426);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(223, 57);
            this.button1.TabIndex = 10;
            this.button1.Text = "Grafico pika";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 515);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonConectar);
            this.Controls.Add(this.comboBoxPortas);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonCarregar);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonRelatorio);
            this.Controls.Add(this.buttonNovoPerfil);
            this.Controls.Add(this.comboBoxPerfil);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Tela inicial";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.Enter += new System.EventHandler(this.Form1_Enter);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxPerfil;
        private System.Windows.Forms.Button buttonNovoPerfil;
        private System.Windows.Forms.Button buttonRelatorio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Button buttonCarregar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxPortas;
        private System.Windows.Forms.Button buttonConectar;
        private System.Windows.Forms.Button button1;
    }
}

