﻿using Controle_temperatura.CONTROLLER;
using Controle_temperatura.MODEL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controle_temperatura.VIEW
{
    public partial class viewCadastro : Form
    {
        List<PontosOpModel> listaPontosOp;
        String ldrMin;
        String ldrMax;
        String PWM;
        PontosOpModel modeloPontosOp;
        PerfilModel modeloPerfil;
        PerfilController perfilController;
        PontosOpController pontosOpController;
        Int32 txtLdrMin;
        
        public viewCadastro()
        {
            InitializeComponent();
            listaPontosOp = new List<PontosOpModel>();
            modeloPerfil = new PerfilModel();
            perfilController = new PerfilController();
            pontosOpController = new PontosOpController();
            labelLDRMin.Text = "Insira um valor entre 0% e 100%";
            maskedTextBoxLDRMin.Text = "0";
            //modeloPontosOp = new ModelPontosOp();
        }

        private void buttonPontos_Click(object sender, EventArgs e)
        {

            maskedTextBoxLDRMin.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            maskedTextBoxLDRMax.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            maskedTextBoxPWM.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;

            if (textBoxNome.Text != String.Empty && maskedTextBoxLDRMax.Text != String.Empty && 
                maskedTextBoxLDRMin.Text != String.Empty && maskedTextBoxPWM.Text != String.Empty)
            {
                int valorPWM = Convert.ToInt16(maskedTextBoxPWM.Text);

                if (maskedTextBoxLDRMax.Text == "100" && valorPWM < 101) {
                    maskedTextBoxLDRMax.Enabled = false;
                    maskedTextBoxPWM.Enabled = false;
                    buttonPontos.Enabled = false;
                    textBoxNome.Enabled = false;
                    labelLDRMin.Text = "Pontos de operações criados com sucesso.";
                    return;
                }
                bool valoresOk = validaValores();

                if (valoresOk)
                {
                    modeloPontosOp = new PontosOpModel();

                    modeloPontosOp.SetLdrMin(Convert.ToInt16(maskedTextBoxLDRMin.Text));
                    modeloPontosOp.SetLdrMax(Convert.ToInt16(maskedTextBoxLDRMax.Text));
                    modeloPontosOp.SetPWM(Convert.ToInt16(maskedTextBoxPWM.Text));

                    listaPontosOp.Add(modeloPontosOp);

                    txtLdrMin = Convert.ToInt32(maskedTextBoxLDRMax.Text) + 1;
                    maskedTextBoxLDRMin.Text = txtLdrMin.ToString();
                    textBoxNome.Enabled = false;
                    labelLDRMin.Text = "Insira um valor entre " + txtLdrMin.ToString() + "% e 100%";
                    //maskedTextBoxLDRMin.Text = txtLdrMin.ToString();
                    
                    maskedTextBoxLDRMax.Text = String.Empty;
                    maskedTextBoxPWM.Text = String.Empty;
                }
                else
                {
                    MessageBox.Show("Valores inseridos incorretamentes.");
                }
            }
            else
            {
                MessageBox.Show("Preencha todos os campos.");
            }

            maskedTextBoxLDRMin.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
            maskedTextBoxLDRMax.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
            maskedTextBoxPWM.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
        }

        private bool validaValores()
        {
            bool retorno = true;
            int tamanhoLista = listaPontosOp.Count;
            int ldrMax;
            int ldrMin;
            try
            {
                ldrMax = Convert.ToInt16(maskedTextBoxLDRMax.Text);
                ldrMin = Convert.ToInt16(maskedTextBoxLDRMin.Text);
            }
            catch
            {
                ldrMax = -1;
                ldrMin = -1;
            }

            if (ldrMax != -1 && ldrMin != -1)
            {
                if(labelAviso1.Visible == false && labelAviso2.Visible == false)
                {
                    if(tamanhoLista > 0)
                    {
                        for (int i = 0; i < tamanhoLista; i++)
                        {
                            String status;
                            if (ldrMin < listaPontosOp[i].GetLdrMax())
                            {
                                //status = "menor";
                                if(!(ldrMax < listaPontosOp[i].GetLdrMin()))
                                {
                                    retorno = false;
                                    break;
                                }
                            }
                            else
                            {
                                //status = "maior";
                                if(!(ldrMax > listaPontosOp[i].GetLdrMin()))
                                {
                                    retorno = false;
                                    break;
                                }
                            }

                        }

                    }
                }
                else
                {
                    retorno = false;
                }

            }
            else
            {
                retorno = false;
            }
            return retorno;
        }
        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void buttonSalvar_Click(object sender, EventArgs e)
        {
            bool okAdd = false;
            if (textBoxNome.Text != String.Empty && listaPontosOp.Count != 0)
            {
                modeloPerfil.SetNome(textBoxNome.Text);
                int codPerfil = perfilController.novoPerfil(modeloPerfil);

                if(codPerfil != -1) {
                    modeloPerfil.SetCodigo(codPerfil);

                okAdd = pontosOpController.novoPontosOp(listaPontosOp, modeloPerfil);
                } else {
                    MessageBox.Show("Erro ao adicionar Perfil");
                }

            }
            else
                MessageBox.Show("Preencha dos os campos e salve pelo menos um ponto de operação");

            if (okAdd) {
                MessageBox.Show("Perfil adicionado com sucesso");                
                this.Close();
            }
        }

        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            maskedTextBoxLDRMin.Text = String.Empty;
            maskedTextBoxLDRMax.Text = String.Empty;
            maskedTextBoxPWM.Text = String.Empty;
            listaPontosOp.Clear();
            this.Hide();
        }

        private void maskedTextBoxLDRMin_Leave(object sender, EventArgs e)
        {
            maskedTextBoxLDRMax.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            maskedTextBoxLDRMin.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            String ldrMax = maskedTextBoxLDRMax.Text;
            String ldrMin = maskedTextBoxLDRMin.Text;
            if(ldrMin != String.Empty)
            {
                int intLdrMin = 0;
                int intLdrMax = 0;
                try
                {
                    intLdrMin = Convert.ToInt16(ldrMin);
                    intLdrMax = Convert.ToInt16(ldrMax);

                }
                catch
                {
                    intLdrMin = -1;
                    intLdrMax = -1;

                }
                if (intLdrMin != -1 && intLdrMax != -1)
                {
                    if (intLdrMin <= 100)
                    {

                        labelAviso2.Visible = false;
                        if (ldrMax != String.Empty)
                        {
                            if (Convert.ToInt16(ldrMax) <= Convert.ToInt16(ldrMin))
                            {
                                labelAviso1.Visible = true;
                            }
                            else
                            {
                                labelAviso1.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        labelAviso2.Visible = true;
                    }
                }
                else
                {
                    labelAviso2.Visible = true;
                }
            }
            maskedTextBoxLDRMax.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
            maskedTextBoxLDRMin.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
        }

        private void maskedTextBoxLDRMax_Leave(object sender, EventArgs e)
        {
            maskedTextBoxLDRMax.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            maskedTextBoxLDRMin.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            String ldrMax = maskedTextBoxLDRMax.Text;
            String ldrMin = maskedTextBoxLDRMin.Text;
            if(ldrMax != String.Empty)
            {
                int intLdrMin;
                int intLdrMax;

                try
                {
                    intLdrMin = Convert.ToInt16(ldrMin);
                    intLdrMax = Convert.ToInt16(ldrMax);
                }
                catch
                {
                    intLdrMin = -1;
                    intLdrMax = -1;
                }
                if (intLdrMin != -1 && intLdrMax != -1)
                {
                    if (intLdrMax <= 100)
                    {
                        if (ldrMin != String.Empty)
                        {
                            labelAviso2.Visible = false;
                            if (intLdrMax <= intLdrMin)
                            {
                                labelAviso1.Visible = true;
                            }
                            else
                            {
                                labelAviso1.Visible = false;
                            }
                        }
                    }else
                    {
                        labelAviso2.Visible = true;
                    }
                }
            }
            maskedTextBoxLDRMax.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
            maskedTextBoxLDRMin.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
        }

        private void maskedTextBoxPWM_Leave(object sender, EventArgs e)
        {
            maskedTextBoxPWM.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            String pwm = maskedTextBoxPWM.Text;

            if(pwm != String.Empty)
            {
                if (Convert.ToInt16(pwm) <= 100)
                {
                    labelAviso2.Visible = false;
                }
                else
                {
                    labelAviso2.Visible = true;
                }
                
            }
        }

        private void viewCadastro_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        private void viewCadastro_FormClosing(object sender, FormClosingEventArgs e)
        {
            textBoxNome.Enabled = true;
            textBoxNome.Text = "";
            labelLDRMin.Text = "Insira um valor entre 0% e 100%";
            maskedTextBoxLDRMin.Text = "0";
            maskedTextBoxLDRMin.Text = "";
            maskedTextBoxLDRMax.Text = "";
            maskedTextBoxPWM.Text = "";
            modeloPerfil.SetNome("");
            modeloPerfil.SetCodigo(-1);
            listaPontosOp.Clear();
            maskedTextBoxLDRMax.Enabled = true;
            buttonPontos.Enabled = true;
            textBoxNome.Enabled = true;
            maskedTextBoxPWM.Enabled = true;

        }
    }
}
