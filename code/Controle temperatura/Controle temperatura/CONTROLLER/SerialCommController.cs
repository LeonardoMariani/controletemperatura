﻿using Controle_temperatura.DAO;
using Controle_temperatura.MODEL;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Controle_temperatura.CONTROLLER {
    class SerialCommController {

        private Thread thread;
        private String nomePerfil;
        private int codUsoPerfil;
        private int codPerfil = -1;
        private String dados;
        private bool conectado = false;
        private bool rodando = true;
        private SerialPort serialPort = new SerialPort();
        private DadosModel modeloDados;
        private DadosDAO dadosDAO = new DadosDAO();
        private List<DadosModel> lista = new List<DadosModel>();

        public void startThread(SerialPort serialPort) {

            this.serialPort = serialPort;

            thread = new Thread(new ThreadStart(leitura));
            thread.Start();
        }
        private void leitura() {

            do {
                 dados = serialPort.ReadExisting();


            } while (dados == String.Empty);


            conectado = true;
            while (rodando) {
                try {
                    dados = serialPort.ReadExisting();
                    if(dados.Count() > 50 && codPerfil != -1 && codUsoPerfil != 0) {
                        adicionarPontos();
                    }
                    
                }catch(Exception e) {

                }
            }
            dados = "";
            conectado = false;
        }

        private void adicionarPontos() {
            modeloDados = new DadosModel();
            string valorLdr = "50";
            int numDigitosLDR = 0;
            string valorPWM = "50";
            string valorPotencia = "50";
            int i = 0;
            int j = 0;
            for (i = 0; i < 20; i++) {
                j = 0;
                modeloDados.SetCodPerfil(codPerfil);
                modeloDados.SetCodUsoPerfil(codUsoPerfil);

                for (int l=0; l< dados.Count(); l++) {
                    l = l + 20;
                    if (dados.ElementAt(j).ToString() != "%") {
                        j++;
                        continue;
                    }
                    if (dados.ElementAt(j).ToString() == "%") {
                        j++;
                        numDigitosLDR =Convert.ToInt16(dados.ElementAt(j).ToString());
                        j++;
                    }
                    if(dados.ElementAt(j).ToString() == ",") {
                        j++;
                        if(numDigitosLDR == 1) {
                            valorLdr = dados.ElementAt(j).ToString();
                            //j++;
                        }else if(numDigitosLDR == 2) {
                            valorLdr = dados.ElementAt(j).ToString() + dados.ElementAt(j + 1).ToString();
                        }else if(numDigitosLDR == 3) {
                            valorLdr = valorLdr = dados.ElementAt(j).ToString() + dados.ElementAt(j + 1).ToString() + dados.ElementAt(j + 2).ToString();
                        }
                    }
                    j++;
                    j++;
                    j++;
                    if (dados.ElementAt(j).ToString() == "+") {
                        j++;
                        valorPWM = "100";
                        j++;
                        j++;
                        j++;
                    }
                    if (dados.ElementAt(j).ToString() == "@") {
                        valorPotencia = "100";
                        j++;
                        j++;
                        j++;
                    }
                    modeloDados.SetLeituraLDR(Convert.ToInt32(valorLdr));
                    modeloDados.SetLeituraPWM(Convert.ToInt16(valorPWM));
                    modeloDados.SetPotenciaMedia(Convert.ToInt16(valorPotencia));
                    lista.Add(modeloDados);                    
                }
            }

            dadosDAO.addDados(lista);
            lista.Clear();
        }

        public void setPerfil(String perfil) {
            this.nomePerfil = perfil;
        }
        public void setUsoPerfil(int cod) {
            this.codUsoPerfil = cod;
        }
        public bool getConectado() {
            return conectado;
        }
        public void stop() {
            rodando = false;
        }
        public void start() {
            rodando = true;
            serialPort.Open();
        }

        internal void setCodPerfil(int codPerfil) {
            this.codPerfil = codPerfil;
        }
    }
}
