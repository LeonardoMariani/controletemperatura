﻿using Controle_temperatura.DAO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace Controle_temperatura.CONTROLLER {
    class RelatorioController {
        DataTable tabela = new DataTable();
        //MySqlDataReader reader = new MySqlDataReader();

        internal DataTable getPerfil() {
            if (tabela != null) {
                tabela.Clear();
            }

            tabela = RelatorioDAO.getPerfil();

            return tabela;
        }

        internal DataTable getConsumo() {
            if (tabela != null) {
                tabela.Clear();
            }

            tabela = RelatorioDAO.getConsumo();

            return tabela;
        }

        internal DataTable getPeriodo(DateTime value1, DateTime value2) {
            if (tabela != null) {
                tabela.Clear();
            }

            tabela = RelatorioDAO.getPeriodo(value1, value2);

            return tabela;
        }

        internal DataTable getGrafico(DateTime inicio, DateTime fim) {
            if(tabela != null) {
                tabela.Clear();
            }

            tabela = RelatorioDAO.getGrafico(inicio, fim);

            return tabela;
        }
    }
}
