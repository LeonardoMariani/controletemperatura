﻿using Controle_temperatura.DAO;
using Controle_temperatura.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_temperatura.CONTROLLER
{
    class PontosOpController
    {
        private PontosOpDAO pontosOpDAO = new PontosOpDAO();

        public bool novoPontosOp(List<PontosOpModel> pontosOp, PerfilModel perfilModel){

            bool retorno = false;

            retorno = pontosOpDAO.novoPonto(pontosOp, perfilModel);

            return retorno;
        }
    }
}
