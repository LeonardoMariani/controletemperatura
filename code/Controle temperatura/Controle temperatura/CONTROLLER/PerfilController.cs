﻿using Controle_temperatura.DAO;
using Controle_temperatura.MODEL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_temperatura.CONTROLLER
{
    class PerfilController{


        private PerfilDAO dao = new PerfilDAO();
        private List<string> listaPerfil = new List<string>();

        public int novoPerfil(PerfilModel modelo)
        {
            int codPerfil = -1;

            codPerfil = dao.novoPerfil(modelo);

            return codPerfil;
        }

        public List<string> getNomePerfil()
        {
            listaPerfil.Clear();

            listaPerfil = dao.getNomePerfil();

            return listaPerfil;

        }

        internal int getCodPerfil(string nomePerfil) {
            int codPerfil;

            codPerfil = dao.getCodigo(nomePerfil);

            return codPerfil;
        }
    }
}
