﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controle_temperatura.MODEL;
using Controle_temperatura.DAO;

namespace Controle_temperatura.CONTROLLER {
    class UsoPerfilController {
        internal int novoUso(UsoModel usoPerfilModel) {
            int codUsoPerfil;

            codUsoPerfil = UsoPerfilDAO.novoUso(usoPerfilModel);

            return codUsoPerfil;
        }

        internal void addTempoFim(int codUsoPerfilAnterior, DateTime tempoFim) {

            UsoPerfilDAO.addTempoFim(codUsoPerfilAnterior, tempoFim);
        }
    }
}
