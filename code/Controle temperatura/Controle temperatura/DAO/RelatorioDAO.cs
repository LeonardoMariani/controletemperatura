﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controle_temperatura.DAO {
    class RelatorioDAO {

        private static string caminho = "server=localhost;user id=root;persistsecurityinfo=True;database=controletemp";
        private static MySqlConnection conexao = new MySqlConnection(caminho);
        private static DataTable table = new DataTable();

        internal static DataTable getPerfil() {

            table.Clear();
            String query = "select distinct Perfil.nome as `Nome Perfil`,(select  sec_to_time((SUM(time_to_sec(usoPerfil.tempoFim) - time_to_sec(usoPerfil.tempoInicio)))) "+
                           "from usoPerfil "+
                           "where Perfil.codPerfil = usoPerfil.codPerfil "+
                           ") as `Tempo ativo` "+
                           "from usoPerfil "+
                           "inner join Perfil "+
                           "on usoPerfil.codPerfil = Perfil.codPerfil;";

            try {

                conexao.Open();

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(table);
            }catch(Exception e) {
                table = null;
            }
            conexao.Close();

            return table;
        }

        internal static DataTable getGrafico(DateTime inicio, DateTime fim) {

            table.Clear();

            String query = "select dados.potenciaMedia,perfil.nome " +
                            " from usoperfil " +
                            " inner join dados" +
                            " on usoperfil.codUsoPerfil = dados.codUsoPerfil and usoperfil.tempoInicio >= @tempoInicio and usoperfil.tempoFim <= @tempoFim " +
                            " inner join perfil" +
                            " on perfil.codPerfil = usoperfil.codPerfil;";
            try {

                conexao.Open();
                MySqlCommand cmd = new MySqlCommand(query, conexao);

                cmd.Parameters.AddWithValue("@tempoInicio", inicio);
                cmd.Parameters.AddWithValue("@tempoFim", fim);

                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(table);

            }catch(Exception e) {
                table = null;
            }
            conexao.Close();

            return table;
        }

        internal static DataTable getPeriodo(DateTime value1, DateTime value2) {

            table.Clear();

            String query = "select distinct Perfil.nome from Perfil "+
                            "inner join usoPerfil"+
                            " on Perfil.codPerfil = usoPerfil.codPerfil "+
                            " where usoPerfil.tempoInicio >= @dataInicio and usoPerfil.tempoFim <= @dataFim;";
            try {

                conexao.Open();

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                cmd.Parameters.AddWithValue("@dataInicio", value1);
                cmd.Parameters.AddWithValue("@dataFim", value2);

                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(table);

            } catch(Exception e) {
                table = null;
            }
            conexao.Close();

            return table;
        }

        internal static DataTable getConsumo() {
            table.Clear();

            String query = " select distinct  Dados.potenciaMedia as `Potencia media`, Perfil.nome as `Nome Perfil`,(select  sec_to_time((SUM(time_to_sec(usoPerfil.tempoFim) - time_to_sec(usoPerfil.tempoInicio)))) " +
                            "from usoPerfil " +
                            "where Perfil.codPerfil = usoPerfil.codPerfil " +
                            ") as `Tempo ativo` " +
                            "from usoPerfil " +
                            "inner join Perfil " +
                            "on usoPerfil.codPerfil = Perfil.codPerfil " +
                            "inner " +
                            "join Dados "+
                            "on Dados.codPerfil = Perfil.codPerfil;";

            try {

                conexao.Open();

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                adapter.Fill(table);
            }catch(Exception e) {
                table = null;
            }
            conexao.Close();
            return table;
        }
    }
}
