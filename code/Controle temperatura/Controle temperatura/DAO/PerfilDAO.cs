﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controle_temperatura.MODEL;
using MySql.Data.MySqlClient;

namespace Controle_temperatura.DAO
{
    class PerfilDAO
    {
        private static string caminho = "server=localhost;user id=root;persistsecurityinfo=True;database=controletemp";
        private static MySqlConnection conexao = new MySqlConnection(caminho);
        private List<string> listaPerfil = new List<string>();

        public int novoPerfil(PerfilModel modelo)
        {
            int codPerfil;

            try {

                conexao.Open();

                string query = "insert into perfil(nome) values(@nome);";
                MySqlCommand cmd = new MySqlCommand(query,conexao);

                cmd.Parameters.AddWithValue("@nome", modelo.GetNome());

                cmd.ExecuteNonQuery();

                conexao.Close();
            } catch(Exception e) {

                conexao.Close();
                return codPerfil = -1;
            }

            codPerfil = getCodigo(modelo.GetNome());

            return codPerfil;
        }

        internal int getCodPerfil(string nomePerfil) {
            int codPerfil;

            try {
                conexao.Open();

                string query = "select codPerfil from perfil where nome = @nome ";

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                cmd.Parameters.AddWithValue("@nome", nomePerfil);

                MySqlDataReader retorno = cmd.ExecuteReader();
                if (retorno.HasRows) {
                    string cod = retorno["codPerfil"].ToString();
                }
                codPerfil = 1;
            } catch(Exception e) {
                codPerfil = -1;
            }

            return codPerfil;
        }

        public List<string> getNomePerfil()
        {
            listaPerfil.Clear();

            try {

                conexao.Open();

                string query = "select nome from perfil;";

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                MySqlDataReader retorno = cmd.ExecuteReader();

                if (retorno.HasRows) {
                    while (retorno.Read()) {
                        String nome = retorno["nome"].ToString();
                        listaPerfil.Add(nome);
                    }
                }

            }catch(Exception e) {

            }
            conexao.Close();

            return listaPerfil;
        }

        public int getCodigo(string nome)
        {
            int cod;

            try {

                conexao.Open();

                string query = "select codPerfil from perfil where nome = @nome";

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                cmd.Parameters.AddWithValue("@nome", nome);

                MySqlDataReader retorno = cmd.ExecuteReader();
                retorno.Read();

                cod = Convert.ToInt16(retorno.GetValue(0).ToString());

            } catch {
                cod = -1;
            }
            conexao.Close();

            return cod;
        }
    }
}
