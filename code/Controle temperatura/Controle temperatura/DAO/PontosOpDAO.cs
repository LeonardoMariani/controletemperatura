﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controle_temperatura.MODEL;
using MySql.Data.MySqlClient;

namespace Controle_temperatura.DAO
{
    class PontosOpDAO
    {
        private static string caminho = "server=localhost;user id=root;persistsecurityinfo=True;database=controletemp";
        private static MySqlConnection conexao = new MySqlConnection(caminho);


        public bool novoPonto(List<PontosOpModel> pontosOp, PerfilModel perfilModel)
        {
            bool retorno = true;

            try {

                conexao.Open();

                for(int i = 0; i < pontosOp.Count; i++) {
                    string query = "insert into pontosoperacao(codPerfil, ldrMin, ldrMax, PWM) values(@codPerfil, @ldrMin, @ldrMax, @PWM);";

                    MySqlCommand cmd = new MySqlCommand(query, conexao);

                    cmd.Parameters.AddWithValue("@codPerfil", perfilModel.GetCodigo());
                    cmd.Parameters.AddWithValue("@ldrMin", pontosOp[i].GetLdrMin());
                    cmd.Parameters.AddWithValue("@ldrMax", pontosOp[i].GetLdrMax());
                    cmd.Parameters.AddWithValue("@PWM", pontosOp[i].GetPWM());

                    cmd.ExecuteNonQuery();
                }
            } catch(Exception e) {
                retorno = false;
            }
            conexao.Close();

            return retorno;
        }
    }
}
