﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controle_temperatura.MODEL;
using MySql.Data.MySqlClient;

namespace Controle_temperatura.DAO {
    class UsoPerfilDAO {

        private static string caminho = "server=localhost;user id=root;persistsecurityinfo=True;database=controletemp";
        private static MySqlConnection conexao = new MySqlConnection(caminho);

        internal static int novoUso(UsoModel usoPerfilModel) {
            int codUso = -1;
            bool insertOk;

            try {

                conexao.Open();

                string query = "insert into usoPerfil(codPerfil,tempoInicio) values(@codPerfil, @tempoInicio);";

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                cmd.Parameters.AddWithValue("@codPerfil", usoPerfilModel.GetCodigo());
                cmd.Parameters.AddWithValue("@tempoInicio", usoPerfilModel.GetTempoInicio());

                cmd.ExecuteNonQuery();
                insertOk = true;

            } catch(Exception e) {
                insertOk = false;
            }

            if (insertOk) {
                string query = "select codUsoPerfil from usoPerfil order by codUsoPerfil desc limit 1;";

                MySqlCommand cmd2 = new MySqlCommand(query, conexao);
                MySqlDataReader retorno = cmd2.ExecuteReader();
                retorno.Read();
                codUso = Convert.ToInt16(retorno.GetValue(0).ToString());
            }
            conexao.Close();

            return codUso;
        }

        internal static void addTempoFim(int codUsoPerfilAnterior, DateTime tempoFim) {

            try {
                string query = "update usoPerfil set tempoFim = @tempoFim where codUsoPerfil = @cod;";
                conexao.Open();

                MySqlCommand cmd = new MySqlCommand(query, conexao);

                cmd.Parameters.AddWithValue("@tempoFim", tempoFim);
                cmd.Parameters.AddWithValue("@cod", codUsoPerfilAnterior);

                cmd.ExecuteNonQuery();
            }catch(Exception e) {

            }
            conexao.Close();
        }
    }
}
