﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Controle_temperatura.MODEL;
using MySql.Data.MySqlClient;

namespace Controle_temperatura.DAO {
    class DadosDAO {

        private static string caminho = "server=localhost;user id=root;persistsecurityinfo=True;database=controletemp";
        private static MySqlConnection conexao = new MySqlConnection(caminho);

        internal void addDados(List<DadosModel> lista) {


            try {
                conexao.Open();

                for(int i = 0; i < lista.Count(); i++) {
                    string query = "insert into dados(codPerfil, valorLDR, valorPWM, potenciaMedia, codUsoPerfil) values(@codPerfil, @valorLDR, @valorPWM, @potenciaMedia, @codUsoPerfil);";

                    MySqlCommand cmd = new MySqlCommand(query, conexao);

                    cmd.Parameters.AddWithValue("@codPerfil", lista[i].GetCodPerfil());
                    cmd.Parameters.AddWithValue("@valorLDR", lista[i].GetLeituraLDR());
                    cmd.Parameters.AddWithValue("@valorPWM", lista[i].GetLeituraPWM());
                    cmd.Parameters.AddWithValue("@potenciaMedia", lista[i].GetPotenciaMedia());
                    cmd.Parameters.AddWithValue("@codUsoPerfil", lista[i].GetCodUsoPerfil());

                    cmd.ExecuteNonQuery();
                }
            }catch(Exception e) {

            }
            conexao.Close();
        }
    }
}
